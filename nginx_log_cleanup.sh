#!/bin/sh
# Đường dẫn đến thư mục log của Nginx
LOG_DIR="/var/log/nginx"

# Xóa các file log có đuôi từ .log.4.gz trở đi
find $LOG_DIR -type f -regex '.*\.log\.[4-9]\.gz' -exec rm -f {} \;



# Truncate log file của container website-vietnampost-frontend_web_1
truncate -s 0 $(docker inspect --format='{{.LogPath}}' website-vietnampost-frontend_web_1)

# Truncate log file của container vietnampost_cms_vnpost-backend-dev_1
truncate -s 0 $(docker inspect --format='{{.LogPath}}' vietnampost_cms_vnpost-backend-dev_1)
